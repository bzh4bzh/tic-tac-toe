#!/usr/bin/env python3
from appJar import gui


app = gui("Tic-Tac-Toe", "300x300")


class Game:

    def __init__(self):
        self.__xTurn = True
        self.__default = chr(127288)
        self.__board = {
            1:self.__default,
            2:self.__default,
            3:self.__default,
            4:self.__default,
            5:self.__default,
            6:self.__default,
            7:self.__default,
            8:self.__default,
            9:self.__default
        }
        self.again = True

    def __playagain(self):
        if self.again:
            setup()
            self.__init__()
        else:
            app.stop()

    def __draw(self):
        '''If no space left and all win possibilities were checked then it must be a draw'''
        for k in self.__board:
            if self.__board[k] == self.__default:
                return False
        return True

    def __winner(self):
        # Horizontal
        if self.__board[1]==self.__board[2]==self.__board[3] and self.__board[1] != self.__default:
            self.again = app.yesNoBox("Game Over", f"{self.__board[1]} wins! Play Again?")
            self.__playagain()
        elif self.__board[4]==self.__board[5]==self.__board[6] and self.__board[4] != self.__default:
            self.again = app.yesNoBox("Game Over", f"{self.__board[4]}  wins! Play Again?")
            self.__playagain()
        elif self.__board[7]==self.__board[8]==self.__board[9] and self.__board[7] != self.__default:
            self.again = app.yesNoBox("Game Over", f"{self.__board[7]}  wins! Play Again?")
            self.__playagain()
        # Vertical
        elif self.__board[1]==self.__board[4]==self.__board[7] and self.__board[1] != self.__default:
            self.again = app.yesNoBox("Game Over", f"{self.__board[1]}  wins! Play Again?")
            self.__playagain()
        elif self.__board[2]==self.__board[5]==self.__board[8] and self.__board[2] != self.__default:
            self.again = app.yesNoBox("Game Over", f"{self.__board[2]}  wins! Play Again?")
            self.__playagain()
        elif self.__board[3]==self.__board[6]==self.__board[9] and self.__board[3] != self.__default:
            self.again = app.yesNoBox("Game Over", f"{self.__board[3]}  wins! Play Again?")
            self.__playagain()
        # Diagonal
        elif self.__board[1]==self.__board[5]==self.__board[9] and self.__board[1] != self.__default:
            self.again = app.yesNoBox("Game Over", f"{self.__board[1]}  wins! Play Again?")
            self.__playagain()
        elif self.__board[3]==self.__board[5]==self.__board[7] and self.__board[3] != self.__default:
            self.again = app.yesNoBox("Game Over", f"{self.__board[3]}  wins! Play Again?")
            self.__playagain()
        elif self.__draw():
            self.again = app.yesNoBox("Game Over", "It's a draw! Play Again?")
            self.__playagain()
        else:
            # No winner, game not over, continue playing
            return

    def press(self, btn):
        if btn is '1':
            if self.__xTurn:
                app.addLabel('L1', text='X', row=0, column=0)
                self.__board[1] = 'X'
                self.__xTurn = False
            else:
                app.addLabel('L1', 'O', row=0, column=0)
                self.__board[1] = 'O'
                self.__xTurn = True
        if btn is '2':
            if self.__xTurn:
                app.addLabel('L2', text='X', row=0, column=1)
                self.__board[2] = 'X'
                self.__xTurn = False
            else:
                app.addLabel('L2', 'O', row=0, column=1)
                self.__board[2] = 'O'
                self.__xTurn = True
        if btn is '3':
            if self.__xTurn:
                app.addLabel('L3', text='X', row=0, column=2)
                self.__board[3] = 'X'
                self.__xTurn = False
            else:
                app.addLabel('L3', 'O', row=0, column=2)
                self.__board[3] = 'O'
                self.__xTurn = True
        if btn is '4':
            if self.__xTurn:
                app.addLabel('L4', text='X', row=1, column=0)
                self.__board[4] = 'X'
                self.__xTurn = False
            else:
                app.addLabel('L4', 'O', row=1, column=0)
                self.__board[4] = 'O'
                self.__xTurn = True
        if btn is '5':
            if self.__xTurn:
                app.addLabel('L5', text='X', row=1, column=1)
                self.__board[5] = 'X'
                self.__xTurn = False
            else:
                app.addLabel('L5', 'O', row=1, column=1)
                self.__board[5] = 'O'
                self.__xTurn = True
        if btn is '6':
            if self.__xTurn:
                app.addLabel('L6', text='X', row=1, column=2)
                self.__board[6] = 'X'
                self.__xTurn = False
            else:
                app.addLabel('L6', 'O', row=1, column=2)
                self.__board[6] = 'O'
                self.__xTurn = True
        if btn is '7':
            if self.__xTurn:
                app.addLabel('L7', text='X', row=2, column=0)
                self.__board[7] = 'X'
                self.__xTurn = False
            else:
                app.addLabel('L7', 'O', row=2, column=0)
                self.__board[7] = 'O'
                self.__xTurn = True
        if btn is '8':
            if self.__xTurn:
                app.addLabel('L8', text='X', row=2, column=1)
                self.__board[8] = 'X'
                self.__xTurn = False
            else:
                app.addLabel('L8', 'O', row=2, column=1)
                self.__board[8] = 'O'
                self.__xTurn = True
        if btn is '9':
            if self.__xTurn:
                app.addLabel('L9', text='X', row=2, column=2)
                self.__board[9] = 'X'
                self.__xTurn = False
            else:
                app.addLabel('L9', 'O', row=2, column=2)
                self.__board[9] = 'O'
                self.__xTurn = True
        self.__winner()


def setup():
    app.removeAllWidgets()
    global g
    g = Game()
    app.addNamedButton('Click me', '1', g.press, row=0, column=0)
    app.addNamedButton('Click me', '2', g.press, row=0, column=1)
    app.addNamedButton('Click me', '3', g.press, row=0, column=2)

    app.addNamedButton('Click me', '4', g.press, row=1, column=0)
    app.addNamedButton('Click me', '5', g.press, row=1, column=1)
    app.addNamedButton('Click me', '6', g.press, row=1, column=2)

    app.addNamedButton('Click me', '7', g.press, row=2, column=0)
    app.addNamedButton('Click me', '8', g.press, row=2, column=1)
    app.addNamedButton('Click me', '9', g.press, row=2, column=2)


g = Game()  # Initialize g.again
while g.again:
    setup()
    app.go()
